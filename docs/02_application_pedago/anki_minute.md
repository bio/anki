---
author: Patrice Hardouin
title: Cartes Anki-minute
tags:
    - activité pédagogique
    - activité minute
    - révisions
---

!!! tip "Objectif de l'activité Anki-minute"

    L'objectif de l'activité «Cartes Anki minute» est de réaliser un lot de cartes Anki en un temps réduit. Cette activité peut-être réalisées en complète autonomie par les apprenants ou sous la supervision de leur professeur.

    ![](./documents/anki_minute.png){ width=50% }
    
    *L'activité Anki-minute (généré avec le [Générateur de Grisebouille](https://framalab.org/gknd-creator))*

## Méthode

- Le professeur vous donne **un thème** sur lequel vous devez élaborer une ou plusieurs fiches Anki ;
- Il faut être **concis et efficace** pour que l'information que vous cherchez à transmettre tienne **en quelques mots** ;
- Vous devrez impérativement **présenter votre production à, au moins, 3 camarades** extérieurs à votre groupe pour vérifier que votre carte Anki est compréhensible avant de la présenter à l'ensemble de la classe ;
- **Produire une carte Anki propre** sur un Pad de l'ENT (ou sur un [Etherpad](https://framapad.org/)) et, le cas échéant, sur l'[application Anki](https://apps.ankiweb.net/docs/manual.fr.html) de l'ordinateur mis à disposition dans la salle.

!!! example "Rôles envisageables des apprenants et du professeur"

    Élaborer ses propres questions/réponses est particulièrement intéressant pour la mémorisation. Si, ensuite, ces questions se retrouvent dans un paquet de cartes Anki alors l'apprenant a toutes les cartes en main (c'est le cas de le dire) pour mémoriser correctement.

    Formuler correctement une question ainsi que sa réponse n'est pas chose aisée et nécessite un apprentissage.

    Par exemple il est possible de dynamiser cette étape en réalisant un document collaboratif pour éviter les questions en double sur un même chapitre. L'utilisation d'un pad collaboratif (celui de l'ENT fera très bien l'affaire ou alors sur Framapad) permet aux apprenants de rédiger des questions et réponses, puis au professeur de proposer des axes de correction avant de pouvoir rapatrier ces questions dans Anki.

    ![](./documents/capture_etherpad.png)
    *Rédaction de questions/réponses de manière collaborative dans Etherpad*

    À ce niveau il est intéressant de procéder par étapes :

    - demander aux apprenants d'imaginer une question/réponse sur un point du chapitre en cours (on peut commencer simple avec des définitions de termes par exemple) ;
    - ouvrir un pad et laisser les apprenants saisir leurs questions/réponse (le fait d'être plusieurs en simultané oblige à la rapidité car les doublons ne sont pas autorisés) ;
    - proposer des corrections de contenu et de logique par le professeur, puis par d'autres apprenants modérateurs ;
    - améliorer les questions par l'[utilisation de verbes d'actions](./documents/Glossaire_verbes_d_action_420261.pdf) ;
    - copier-coller les questions-réponses pertinentes dans un jeu de cartes Anki ([il est conseillé d'utiliser les marqueurs pour classer le contenu du jeu de cartes](https://apps.ankiweb.net/docs/manual.fr.html#changer-de-paquet-ou-de-type-de-note)) ;
    - diffuser le jeu de carte en question à toute la classe via l'ENT par exemple.

## Les trois modèles de cartes

!!! example "Le modèle «**Quiz**»"

    ![](./documents/quiz.png){ width=30%; align=right }

    - Les cartes «**Question/Réponse**» doivent utiliser les [verbes d'actions](./documents/Glossaire_verbes_d_action_420261.pdf) pour la question.

!!! example "Le modèle «**Définition**»"

    ![](./documents/Definition.png){ width=30%; align=right }

    - Les termes utilisés sur la face avant de la carte ne doivent pas se retrouver sur la face arrière ; cela permet de créer des cartes automatiquement pour retrouver le terme lié à une **définition** qui s'affiche par exemple.


!!! example "Le modèle «**Acronyme**»"

    ![](./documents/Acronyme.png){ width=30%; align=right }

    - Les cartes «**Acronymes**» ou «Sigles» possèdent 3 champs qui seront détaillés plus avant dans l'exemple présenté plus bas.

## Exemple d'utilisation du modèle Quiz

Les notes de type «Quiz» possèdent deux champs à compléter

![](./documents/quiz.png)

Ce type de modèle de note permet de créer automatiquement une unique carte de révision. C'est le modèle le plus simple.

![](./documents/Ques_Repo.png)


## Exemple d'utilisation du modèle Définition

Les notes de type «Définition» possèdent deux champs à compléter

![](./documents/Definition.png)

Ce type de modèle de note permet de créer automatiquement 2 cartes de révision différentes. Le contenu de chacune des lignes doit donc être pensé pour satisfaire à cette fonctionnalité particulière :

![](./documents/Defi_Term.png){ width=25% }
![](./documents/Term_Defi.png){ width=25% }

## Exemple d'utilisation du modèle Acronyme

Les notes de type «Acronyme» possèdent trois champs à compléter :

![](./documents/Acronyme.png)

Ce type de modèle de note permet de créer automatiquement 6 cartes de révision différentes. Le contenu de chacune des lignes doit donc être pensé pour satisfaire à cette fonctionnalité particulière :

![](./documents/Acro_Sign.png){ width=25% }
![](./documents/Acro_Defi.png){ width=25% }

![](./documents/Sign_Acro.png){ width=25% }
![](./documents/Acro_Sign.png){ width=25% }

![](./documents/Defi_SIgn.png){ width=25% }
![](./documents/Sign_Defi.png){ width=25% }


## Récupérer les modèles de notes pour pouvoir les utiliser directement

- Pour bénéficier de ces trois modèles sur votre instance du logiciel Anki, vous pouvez télécharger ce paquet spécialement élaboré pour vous et l'importer dans votre logiciel : [Types de notes pour Anki](../01_premiers_pas/documents/Types_de_notes.apkg) ou directement depuis le cloud Anki : [https://ankiweb.net/shared/info/1139782677](https://ankiweb.net/shared/info/1139782677)
- Vous pouvez également réaliser vos propres modèles en [suivant les indications de la documentation d'ANKI](https://apps.ankiweb.net/docs/manual.fr.html).

<iframe title="Prérequis pour une prise en main rapide d'ANKI" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/d2fecec7-d1ff-4602-9631-94d939d20798" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

<iframe title="Importer un paquet de cartes Anki partagé" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/fc725cd0-92fe-4be6-b0b9-bf41ebd05d03" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>