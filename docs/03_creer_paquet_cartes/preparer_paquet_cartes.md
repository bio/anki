---
author: Patrice Hardouin
title: Préparer rapidement un paquet de cartes de révision
tags:
    - révisions
    - compléments du cours
    - mémorisation
---

![](./documents/creation_paquet_cartes_Anki.png){ width=50% }
 
*Créer un paquet de cartes pour Anki (généré avec le [Générateur de Grisebouille](https://framalab.org/gknd-creator))*

!!! warning "Ce tutoriel a été réalisé pour proposer une solution rapide"

    La solution choisie pour rédiger rapidement des paquets de cartes Anki passe ici par un éditeur de texte. Cela n'est pas la seule méthode utilisable pour créer ses cartes de mémorisation. Le logiciel Anki propose notamment des outils pour créer des cartes une par une, comme tu peux le constater dans la vidéo qui suit.

    <iframe title="Création d'une nouvelle carte dans ANKI" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/ab8f93dd-59c0-4140-9c4e-928f472b6c76" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

!!! note "Télécharger et installer le paquet de modèles de notes"

    Il est conseillé, avant toute chose, de télécharger et d'installer le paquet contenant les trois modèles de notes (Quiz, Définition et Acronyme) : [Types de notes pour Anki](../01_premiers_pas/documents/Types_de_notes.apkg) ou directement depuis le cloud Anki : [https://ankiweb.net/shared/info/1139782677](https://ankiweb.net/shared/info/1139782677)

    <iframe title="Prérequis pour une prise en main rapide d'ANKI" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/d2fecec7-d1ff-4602-9631-94d939d20798" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>


!!! tip "Vous avez 4 minutes devant vous ? Alors c'est parti !"

    Pour créer rapidement vos cartes pour Anki : ouvrez un simple éditeur de texte comme Notepad ou [Etherpad](https://framapad.org/abc/fr/) et créez votre liste de cartes, ligne après ligne, tout simplement, comme expliqué dans le tutoriel vidéo.

    Ce court tutoriel montre comment créer ses fiches de révisions de vocabulaire depuis un simple éditeur de texte. Ces fiches seront à destination du logiciel de mémorisation par répétitions espacées Anki. Les cartes Anki pourront être également agrémentées d'illustrations.

    - La syntaxe, par ligne, pour votre fichier texte est : **terme;définition;étiquette**. Chaque ligne sera une carte Anki.
    - À la suite de la définition, le code HTML pour insérer une image après un saut à la ligne est ``</br><img src="">`` et vous copiez le lien vers l'image présente sur internet, entre les deux guillemets.
    - **Enregistrez le fichier produit avec l'extension .txt** pour qu'il soit reconnu par Anki

    <iframe title="Création rapide d'un paquet de cartes ANKI dans un éditeur de texte" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/351fbe62-e12b-4dc5-8ce8-cf66aa53898c" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

    Ce travail de regroupement des définitions constitue déjà une bonne partie de l'apprentissage. Récupérer les cartes Anki réalisées par quelqu'un d'autre est efficace mais pas autant que de créer vos propres fiches. Malgré tout, si le temps vous est compté, vous pouvez travailler à plusieurs pour générer un paquet de cartes, chacun traitant une partie des définitions en utilisant, par exemple, [Framapad](https://framapad.org/abc/fr/).

    Le [fichier texte produit dans cet exemple est téléchargeable ici-même](./documents/Biologie_gymnasiale.txt). Vous pouvez l'utiliser pour faire éventuellement un essai d'importation dans Anki.

    Pour plus de détails sur la syntaxe utilisable dans les fichiers d'importation : [https://apps.ankiweb.net/docs/manual.fr.html#importation-de-fichiers-texte](https://apps.ankiweb.net/docs/manual.fr.html#importation-de-fichiers-texte)


!!! info "Importer ses cartes de mémorisation dans Anki et les utiliser"

    Le fichier texte créé à l'étape précédente peut directement être importé dans Anki pour être immédiatement utilisé en révisions.

    Ce tutoriel explique comment importer un fichier texte réalisé rapidement.

    <iframe title="Importer un fichier texte de cartes dans ANKI" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/c244fa6d-a902-4c9f-baf1-afdde0ff53b9" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

    Voilà, vous êtes maintenant paré pour réviser votre vocabulaire et vos définitions incontournables afin de réussir votre examen avec brio.

    Si vous souhaitez générer un paquet de cartes au format propre à ANKI pour le partager aisément vous pouvez le faire aisément en suivant la courte vidéo qui suit. Par exemple, le [fichier .apkg produit dans le cadre de ce tutoriel est téléchargeable ici-même](./documents/Biologie_gymnasiale.apkg). Vous pouvez l'utiliser dans le cadre de vos essais d'importation.

    <iframe title="Exporter son paquet de cartes au format Anki pour le partager" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/896adc45-85d2-408c-bf39-9f89f49f9e9d" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>