---
author: Patrice Hardouin
title: Prise en main d'ANKI
tags:
    - intitiation
    - découverte
    - mémorisation
---

!!! info "Imaginez une simple carte..."

    Sur cette carte figure une question : « Nommer trois familles de macronutriments énergétiques ». Vous réfléchissez puis tentez de verbaliser la réponse : « Glucides, lipides, protéines ».

    Vous retournez alors la carte et constatez que la réponse est effectivement : « Glucides, lipides, protéines ».

    En face de vous se trouvent trois boîtes : « À revoir, correct, facile ». Vous jetez alors cette carte directement dans la boîte « facile ».

    ![](./documents/fonctionnement_ANKI.png){ width=50% }

    *Principe d'utilisation d'Anki (généré avec le [Générateur de Grisebouille](https://framalab.org/gknd-creator))*

## Découvrir ANKI

Anki est un logiciel qui va vous permettre de faire comme dans la situation précédente mais directement sur un ordinateur ou votre smartphone.

Lorsque vous étudiez un jeu de cartes Anki, vous les placez virtuellement dans les différentes boîtes en fonction de vos réponse. C'est là que le logiciel est intéressant : il vous re-proposera plus rapidement les cartes déposées dans la boîte « À revoir » et cette méthode (la répétition espacée) est très efficace pour l'apprentissage.

Ainsi vous pourrez tout réviser :

- des définitions (dans toutes les matières…) ;
- des formules (de mathématique, de chimie, de gestion…) ;
- du vocabulaire (langue vivante, lexique technique…) ;
- des gestes (pratique professionnelle, activité physique comme les arts martiaux…) ;
- des tablatures de guitare…
- …

Poursuivez votre découverte d'Anki en consultant la [documentation francophone](https://apps.ankiweb.net/docs/manual.fr.html).

!!! tip "Comment se procurer Anki ?"

    - Il est conseillé de commencez par [installer ANKI sur un ordinateur](https://apps.ankiweb.net/) ; vous allez pouvoir y ajouter le [paquet de notes de démarrage](https://bio.forge.apps.education.fr/anki/res/Types_de_notes.apkg) et commencer à [créer votre premier paquet de cartes](../03_creer_paquet_cartes/).
    - Ensuite, créez votre compte sur Ankiweb ; cette solution est très pratique car votre compte sera synchronisé dans le cloud et vous pourrez poursuivre à la maison un travail de révision commencé sur les ordinateurs du CDI par exemple, en [synchronisant votre logiciel ANKI avec votre compte Ankiweb](https://apps.ankiweb.net/docs/manual.fr.html#synchronisation-avec-ankiweb).
    - Installez [Anki sur votre smartphone Android](https://apps.ankiweb.net/) ou utilisez directement [Ankiweb](https://ankiweb.net/about) sur votre IPhone (la version [Apps d'Anki est payante sur IPhone](https://apps.ankiweb.net/)).
    - Partagez vos jeux de cartes ANKI, échangez-les et travaillez à plusieurs sur un même paquet sont des possibilités qui s'offrent alors à vous pour élaborer des ressources de révision qui vous seront pleinement adaptées.



## Élaborer ses propres cartes de révision pour Anki

!!! warning "Télécharger et installer le paquet de modèles de notes"

    Il est conseillé, avant toute chose, de télécharger et d'installer le paquet contenant les trois modèles de notes (Quiz, Définition et Acronyme) : [Types de notes pour Anki](./documents/Types_de_notes.apkg) ou directement depuis le cloud AnkiWeb [https://ankiweb.net/shared/info/1139782677](https://ankiweb.net/shared/info/1139782677)

    <iframe title="Prérequis pour une prise en main rapide d'ANKI" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/d2fecec7-d1ff-4602-9631-94d939d20798" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

!!! tip "Créer une carte en quelques secondes"    

    <iframe title="Création d'une nouvelle carte dans ANKI" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/ab8f93dd-59c0-4140-9c4e-928f472b6c76" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

    Pour créer un paquet de carte de révision pour Anki, rien de plus simple :

    - ouvrir votre logiciel Anki (AnkiDroid, la version Linux / MacOS / MS Windows ou encore la version web : ankiweb) ;
    - choisir «Ajouter» ;
    - puis créer un nouveau paquet ou utiliser le paquet existant ;
    - une fiche vierge apparaît et il suffit de remplir les champs «Terme» et «Définition» (si vous avez sélectionné la note «Définition») avec ce que vous voulez faire apparaître sur la face avant ou arrière de votre première fiche ; vous pouvez sélectionner le modèle de note en haut de la fenêtre ([quiz, définition ou acronyme](../02_application_pedago/)) ;
    - Dans la vidéo, j'ai choisi de rédiger le contenu de notre fiche, mais il est tout à fait possible de copier/coller du texte et/ou des images ;
    - En cliquant sur le bouton «Ajouter» nous allons pouvoir créer de nouvelles fiches pour compléter notre paquet et commencer nos révisions efficacement.

    Trouvez plus d'informations sur les paramétrages des cartes d'Anki (ajout de son, vidéo…) en consultant la [documentation francophone](https://apps.ankiweb.net/docs/manual.fr.html).

!!! note "Créer tout un lot de cartes en une seule manipulation"

    Une autre méthode de création de cartes Anki (en utilisant un simple éditeur de texte) est présenté dans le tutoriel [Préparer rapidement un paquet de cartes de révision](../03_creer_paquet_cartes/)


## Synchronisation et partage d'un paquet de cartes ANKI

ANKI permet d'étudier ses paquets de cartes sur toutes les instances que vous souhaitez utiliser. Pour cela, Anki utilise un cloud qui stocke vos cartes et votre progression. Ainsi, vous pouvez commencer à étudier sur votre smartphone et poursuivre sur l'application Web, avant de passer un peu de temps sur votre ordinateur portable…

![](./documents/CloudAnki.svg)

*Principe de la synchronisation des instances ANKI*

Pour synchroniser ses instances ANKI et pour partager un paquet de cartes Anki il faut avoir créé un compte (libre et gratuit) sur [ankiweb.net](https://ankiweb.net/) et l'avoir paramétré dans ses [applications Anki](https://apps.ankiweb.net/).

![](./documents/synchroniser_cloud_anki.png)

*Synchroniser ses paquets de cartes dans le cloud d'ANKI*

Le partage d'un paquet que vous auriez créé (et que vous souhaitez proposer à tout le monde sous licence libre) se fait sur AnkiWeb en cliquant sur «Actions» puis «Share> en bout de ligne, à la suite du nom de votre paquet :

![](./documents/partage_depuis_Ankiweb.png)

*Partage de paquet depuis AnkiWeb*

## Importer un paquet de cartes partagé

Pour importer un paquet partagé sur internet il suffit de sélectionner «Get shared decks» sur AnkiWeb ou «Partages» sur l'application qui est francisée. Une fois téléchargé il s'installera facilement dans l'application Anki (choisir alors «importer» et sélectionner le fichier fraîchement téléchargé).

<iframe title="Importer un paquet de cartes Anki partagé" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/fc725cd0-92fe-4be6-b0b9-bf41ebd05d03" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>

Vous pouvez vous entraîner à l'importation du paquet de cartes de démarrage qui est présent sur le cloud d'AnkiWeb  : [https://ankiweb.net/shared/info/1139782677](https://ankiweb.net/shared/info/1139782677)