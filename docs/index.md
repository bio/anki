---
author: Patrice HARDOUIN
title: 🏡 Accueil
---

# Réaliser des cartes de révisions pour Anki et les utiliser

![](./assets/images/choisir_anki_pour_reviser.png){ width=50% }

*Révise avec Anki (généré avec le [Générateur de Grisebouille](https://framalab.org/gknd-creator))*

3 tutoriels pour découvrir et utiliser Anki dans ses révisions :

- [Premiers pas avec ANKI](https://bio.forge.apps.education.fr/anki/01_premiers_pas/decouvrir_anki/)
- [Application pédagogique](https://bio.forge.apps.education.fr/anki/02_application_pedago/anki_minute/)
- [Générer son propre paquet de cartes pour réviser](https://bio.forge.apps.education.fr/anki/03_creer_paquet_cartes/preparer_paquet_cartes/)

