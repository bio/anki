# Tutoriels sur ANKI

Réaliser des cartes de révisions pour Anki et les utiliser 

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table des contenus** 

- [Les tutoriels ANKI](#les-tutoriels-anki)
- [Hébergement des vidéos](#hébergement-des-vidéos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Les tutoriels ANKI

Ces tutoriels sont accessibles en consultation à l'adresse [https://bio.forge.apps.education.fr/anki](https://bio.forge.apps.education.fr/anki)

- Trois tutoriels qui se complètent pour être immédiatement opérationnel dans ses révisions grâce à ANKI. 

- Des exemples d'applications pédagogiques en classe sont également présentés. 

- Profitez-en, c'est libre ! 


## Hébergement des vidéos

L'ensemble des vidéos présentées est hébergé sur [la chaîne BioSciences](https://tube-sciences-technologies.apps.education.fr/c/hardouin_patrice_channel).

